Readings
========

## Readings

:::{margin}
When I last checked, it was available for only [$2,242.19 on Amazon!](https://www.amazon.com/Introduction-Renormalization-Group-Methods-Physics/dp/0486793451).
:::
* {cite:p}`Creswick:2016`: A very nice textbook with examples of random walks, the Ising
  model, percolation, Feigenbaum's constant, $\phi^4$ theory and more.  Unfortunately,
  very hard to find, although we do have a copy in our library.
* [John McGreevy's Lecture Notes
  (PDF)](https://mcgreevy.physics.ucsd.edu/f18/2018F-217-lectures.pdf): Based on his
  course [Physics 217, The Renormalization Group, Fall 2018
  (UCSD)](https://mcgreevy.physics.ucsd.edu/f18/), these notes cover and expand upon
  many of the ideas in {cite:p}`Creswick:2016` we discuss here.
* {cite:p}`Lepage:1997`: "How to Renormalize the Schrödinger Equation".  Fantastic tour
  of regularization, renormalization, and the principles of modern effective theory
  using the Schrödinger equation as an example.  I **highly** recommend you work through
  as much of this as possible.
* {cite:p}`Huang:2013`: "A critical history of renormalization".  An interesting
  discussion of renormalization group with regards to the nature of physical theories.
  Additional discussions can be found in {cite:p}`Huang:2007` (ebook available [through
  the WSU
  Library](https://ntserver1.wsulibs.wsu.edu:2171/lib/wsu/reader.action?docID=312372))
  and {cite:p}`Huang:2008` (print book available in the library).


(sec:references)=
References
==========

```{bibliography}
:style: alpha
```
