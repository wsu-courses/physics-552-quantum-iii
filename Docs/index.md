<!-- Phys 552 - Quantum Theory III
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->

# Physics 552 - Quantum Theory III

These are incomplete notes for some guest lectures about the renormalization group for
Physics 552, Quantum Theory III offered in the Spring of 2022 at WSU. If you have
questions or comments about these notes, please contact Michael McNeil
Forbes <mforbes@wsu.edu>.  For details about other aspects of the course, please
contact Nicholas Cerruti <ncerruti@wsu.edu>.




Welcome to Phys 552 - Quantum Theory III!  This is the main documentation page for the
course.  For more class information, please see the {ref}`sec:sylabus`.

Instructors: the information presented here at the start of the course documentation is
contained in the `Docs/index.md` file, which you should edit to provide an overview of
the course.

One reasonable option might be to replace this by a literal include of the top-level
`README.md` file with the following code:

````markdown
```{include} ../README.md
``` 
````

```{toctree}
---
maxdepth: 4
caption: "Contents:"
---
RenormalizationGroup
ClassNotes/RandomWalks
ClassNotes/RenormalizingTheSEQ.md
```

```{toctree}
---
maxdepth: 4
caption: "Appendices:"
---
ClassNotes/RadialSEQ
ClassNotes/Feigenvalue.md
ClassNotes/Renormalization/SUSY-QM.md 
ClassNotes/Renormalization/SUSY-Hydrogen.md 
References
```


<!--
```{toctree}
---
maxdepth: 2
caption: "Miscellaneous:"
hidden:
---
Demonstration
CoCalc
Overview
../InstructorNotes

README.md <../README>
```
-->

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.
     
     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->
