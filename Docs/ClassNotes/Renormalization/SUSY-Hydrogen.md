---
jupytext:
  encoding: '# -*- coding: utf-8 -*-'
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.7
kernelspec:
  display_name: Python 3 (phys-552-2022)
  language: python
  name: phys-552-2022
---

```{code-cell}
:hide-cell: null

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

# Hydrogen-like Atoms with Supersymmetry

Here we use th SUSY method to find the eigenstates of single-electron hydrogen-like
atoms. After dealing with the center-of-mass motion, angular momentum, and introducing
the radial wavefunction $U_{El}(r)$ we have following 1D problem:


\begin{gather*}
  \psi_{Elm}(\vec{x}) = \overbrace{\frac{U_{El}(r)}{r}}^{R_{El}(r)}Y^{m}_{l}(\phi, \theta), \qquad
  \left(\frac{-\hbar^2}{2m}\diff[2]{}{r} + \frac{\hbar^2 l (l+1)}{2mr^2} - \frac{e^2}{r} - E\right)U_{El}(r) = 0,
\end{gather*}

where $m = m_em_p/(m_e+m_p)$ is the reduced mass, and $e^2 = \alpha \hbar c$ is the square of the proton charge expressed in terms of the fine-structure constant $\alpha \approx 1/137$.

Note that we can think of $U_{El}(r) = \braket{r|n,l}$ if we normalize everything on the interval $r \in [0, \infty)$:

\begin{gather*}
  \int_0^{\infty} r^2\d{r} \abs{R_{El}(r)}^2 = \int_0^{\infty} \d{r} \abs{U_{El}(r)}^2.
\end{gather*}

+++

## Units

We start by choosing units so that $\hbar = 2m = e^2/2 =1$.

\begin{gather*}
  [\hbar] = \frac{MD^2}{T}, \qquad [2m] = M, \qquad [e^2] = \frac{MD^3}{T^2}.
\end{gather*}

To reconstruct the dimensionful solution, insert 1 in the following forms to get the correct dimensions:

\begin{gather*}
  1 = \underbrace{\frac{2\hbar^3}{me^4}}_{\text{time}}
    = \underbrace{\frac{\hbar^2}{me^2}}_{\text{distance}}
    = \underbrace{\frac{me^4}{2\hbar^2}}_{\text{energy}}
    = \underbrace{\frac{me^2}{\hbar}}_{\text{momentum}}.
\end{gather*}

Our goal is to find the eigenstates and eigenvalues of the Hamiltonian:

\begin{gather*}
  \op{H}^{(l)} = \op{p}^2 + \frac{l(l+1)}{r^2} - \frac{2}{r}.
\end{gather*}

+++

## Spectrum
We now guess the form of the superpotential:

\begin{gather*}
  W_l(r) = \alpha + \frac{\beta}{r}, \qquad
  W_l'(r) = \frac{-\beta}{r^2}, \\
  V_1 = W_l^2(r) - W_l'(r) = \frac{\beta(\beta+1)}{r^2} + \frac{2\alpha\beta}{r} + \alpha^2
  = \frac{l(l+1)}{r^2} - \frac{2}{r} - E_0.  
\end{gather*}

Thus, $\alpha\beta = -1$, $E_0 = -\alpha^2$, and $\beta \in \{l, -l-1\}$.  We will proceed with the choice of $\beta = -(1+l)$ to make $W'_l(r) = (l+1)/r > 0$ since this should increase the energy.  Thus, we have the following super-symmetric pairs

\begin{align}
  V_1 &= W_l^2(r) - W_l'(r) =\frac{l(l+1)}{r^2} - \frac{2}{r} + \frac{1}{(1+l)^2}, & 
  \op{H}_1 &= \op{A}_l^\dagger\op{A}_l = \op{H}^{(l)} + \frac{1}{(1+l)^2}, \\
  V_2 &= W_l^2(r) + W_l'(r) = \frac{(l+1)(l+2)}{r^2} - \frac{2}{r} + \frac{1}{(1+l)^2}, &
  \op{H}_2 &= \op{A}_l\op{A}_l^\dagger =\op{H}^{(l+1)} + \frac{1}{(1+l)^2}.
\end{align}

+++

First, define the spectrum of the Hamiltonian $\op{H}^{(l)}$ to be $E^{(l)}_{s}$ with $E^{(l)}_{0}$ as the ground state, $E^{(l)}_{1}$ the first excited state, etc.  Let us also denote the spectrum of $\op{H}_1$ to be $E_{s}$ where $E_{0}$ is the ground state, $E_{1}$ the first excited state.  To make the argument a little more general, consider the following:

\begin{align}
  \op{H}_1 &= \op{H}^{(l)} + f(l), & f(l) = \frac{1}{(1+l)^2}\\
  \op{H}_2 &= \op{H}^{(l+1)} + g(l), & g(l) = \frac{1}{(1+l)^2}.
\end{align}

For hydrogen, $f(l) = g(l)$, but in general, these may be different.  Now, from the SUSY properties, recall that:

1. $\op{H}_1$ has a zero-energy ground state $E_0 = 0$, and,
2. $\op{H}_2$ has the same energy spectrum as $\op{H}_1$ except for this ground state.  I.e. $E_1$ is the first excited state energy of $\op{H}_1$, but the ground state energy of $\op{H}_2$, etc.

These properties translate directly to the following statements respectively:

\begin{gather*}
  E_0 = 0 = E^{(l)}_{0} + f(l),\\
  E_{s} = E^{(l)}_{s} + f(l) = E^{(l+1)}_{s-1} + g(l).
\end{gather*}

Thus:

\begin{gather*}
  E^{(l)}_{0} = -f(l), \\
  E^{(l)}_{s} = E_{s} - f(l) = E^{(l+1)}_{s-1} + g(l) - f(l).
\end{gather*}

This recursively defines the entire spectrum:

\begin{align}
  E^{(l)}_{s} &= E^{(l+1)}_{s-1} + g(l) - f(l),\\
              &= \overbrace{E^{(l+2)}_{s-2} + g(l+1) - f(l+1)}^{E^{(l+1)}_{s-1}} + g(l) - f(l),\\
              &= \overbrace{E^{(l+3)}_{s-3} + g(l+2) - f(l+2)}^{E^{(l+2)}_{s-2}} + g(l+1) - f(l+1) + g(l) - f(l),\\
              &\qquad \vdots \\
              &= \rlap{E^{(l+s)}_{0}}\phantom{-f(l+s)} + g(l+s-1) - f(l+s-1) + g(l+s-2) - f(l+s-2) + \cdots + g(l) - f(l),\\
              &= -f(l+s) + g(l+s-1) - f(l+s-1) + g(l+s-2) - f(l+s-2) + \cdots + g(l) - f(l).
\end{align}

For hydrogen, this is simple since $f(l) = g(l)$, all terms cancel except the first $E^{(l)}_{s} = -f(l+s)$:

\begin{align}
  E^{(l)}_{0} &= -\frac{1}{(1+l)^2}, \\
  E^{(l)}_{s} &= E^{(l+1)}_{s-1} = \cdots = E^{(l+s)}_{0} = -\frac{1}{(1+l+s)^2}.
\end{align}

We thus have determined the complete spectrum.  *In this case, each $\op{H}^{(l)}$ has the same truncated spectrum, but in other systems like the isotropic harmonic oscillator you will work on in your assignment, there may be shifts between the energies.*

\begin{gather*}
  E^{(l)} = -\frac{1}{(1+l)^2}.
\end{gather*}

This is the ground state energy of the Hamiltonian $\op{H}^{(l)}$, the first excited state of $\op{H}^{(l-1)}$, the second excited state of $\op{H}^{(l-2)}$ etc.:

\begin{align}
  \op{H}^{(0)} &= \op{p}^2 - \frac{2}{r}, & \{E^{(0)}, E^{(1)}, E^{(2)}, E^{(3)}, E^{(4)}, \dots\}&\\
  \op{H}^{(1)} &= \op{p}^2 + \frac{2}{r^2} - \frac{2}{r}, & \{E^{(1)}, E^{(2)}, E^{(3)}, E^{(4)}, \dots\}&\\
  \op{H}^{(2)} &= \op{p}^2 + \frac{6}{r^2} - \frac{2}{r}, & \{E^{(2)}, E^{(3)}, E^{(4)}, \dots\}&\\
  & \vdots\\
  \op{H}^{(l)} &= \op{p}^2 + \frac{l(l+1)}{r^2} - \frac{2}{r}, & \{E^{(l)}, E^{(l+1)}, \dots\}&\\
  & \vdots
\end{align}

+++

## Wavefunctions

+++

We now need a notation for the states.  We shall use the standard notation, where the energy levels are denoted by the **principle quantum number** $n \in \{1,2,3,\dots\}$ and angular momentum quantum number $l \in \{0, 1,\dots, n-1\}$.  Note that the ground state for $\op{H}^{(l)}$ has $n=l+1$ and the overall ground state has $n=1$, not $n=0$ as we often use in other systems.

\begin{gather*}
  \op{H}^{(l)}\ket{n,l} = \ket{n,l}E_{n}, \qquad E_{n} = -\frac{1}{n^2} = E^{(n-1)}.
\end{gather*}

Thus, we have 

\begin{align}
  \op{H}^{(0)} &= \op{p}^2 - \frac{2}{r}, & \{\ket{1,0}, \ket{2,0}, \ket{3,0}, \ket{4,0}, \dots\}&\\
  \op{H}^{(1)} &= \op{p}^2 + \frac{2}{r^2} - \frac{2}{r}, & \{\ket{2,1}, \ket{3,1}, \ket{4,1}, \dots\}&\\
  \op{H}^{(2)} &= \op{p}^2 + \frac{6}{r^2} - \frac{2}{r}, & \{\ket{3,2}, \ket{4,2}, \dots\}&\\
  & \vdots\\
  \op{H}^{(l)} &= \op{p}^2 + \frac{l(l+1)}{r^2} - \frac{2}{r}, & \{\ket{l+1,l}, & \ket{l+2,l}, \dots\}\\
  & \vdots
\end{align}

To move between wavefunctions, we use the operators $\op{A}_{l}$:


\begin{gather*}
  \newcommand\lr[1]{\mathop{\substack{\longrightarrow\\\longleftarrow}}_{\op{A}^\dagger_{#1}}^{\op{A}_{#1}}}
  \newcommand\l[1]{\mathop{\longleftarrow}^{\op{A}^\dagger_{#1}}\;}
  \newcommand\r[1]{\mathop{\longrightarrow}^{\op{A}_{#1}}}
  \begin{aligned}
    &\ket{n,l}&l=0& &l=1& &l=2& &l=3&\\  
    \\
    &\vdots &\vdots\quad& &\vdots\quad& &\vdots\quad& &\vdots\quad&\\
    n&=4 &\ket{4,0}&\r{0} &\l{0}\ket{4,1}&\r{1} &\l{1}\ket{4,2}&\r{2} &\l{2}\ket{4,3}&\r{3}0\\
    n&=3 &\ket{3,0}&\r{0} &\l{0}\ket{3,1}&\r{1} &\l{1}\ket{3,2}&\r{2}0\\
    n&=2 &\ket{2,0}&\r{0} &\l{0}\ket{2,1}&\r{1}0\\
    n&=1 &\ket{1,0}&\r{0}0\\
    \\
    &&\op{H}^{(0)} &&\op{H}^{(1)} &&\op{H}^{(2)} &&\op{H}^{(3)}
  \end{aligned}
\end{gather*}

+++

Thus, the ground states of $\op{H}^{(l)}$ is $\ket{l+1,l}$, which is annihilated by $\op{A}_{l}$, or in terms of the principle quantum number, the state $\ket{n,n-1}$ is the ground state of $\op{H}^{(n-1)}$ and is annihilated by $\op{A}_{n-1}$:

\begin{gather*}
  \op{A}_{n-1}\ket{n,n-1} = 0,\qquad
  \Biggl(\overbrace{\frac{-n}{r} + \frac{1}{n}}^{W_{n-1}} + \diff{}{r}\Biggr)\psi^{(n-1)}_n(r) = 0\\
  \psi^{(n-1)}_{n}(r) = N_{n-1}\exp\left(-\int^{r} W_{n-1}(r)\right)
                      = N_{n-1}\exp\left(n\ln{r} - \frac{r}{n}\right)
                      = \frac{1}{\sqrt{(2n)!}}\left(\frac{2}{n}\right)^{n+1/2} r^{n}e^{-r/n},
\end{gather*}


where the normalization factor comes from integrating:

\begin{gather*}
  \int_0^{\infty} x^{a}e^{-x}\d{x} = \Gamma(a+1) = a!\\
  \int_0^{\infty} r^{2(l+1)}e^{-2r/(l+1)}\d{r}
  = \left(\frac{l+1}{2}\right)^{2l+3}\int_0^{\infty} x^{2(l+1)}e^{-x}\d{x}
  = \left(\frac{l+1}{2}\right)^{2l+3}(2(l+1))!,\\
\end{gather*}

+++

The other states are obtained with the creation operators:

\begin{gather*}
  \op{A}^{\dagger}_{l}\ket{n,l+1} = \ket{n,l}\sqrt{E_n-E_{l+1}},\\
  \braket{n,l+1|\op{A}_{l}\op{A}^{\dagger}_{l}|n,l+1}
  = \braket{n,l+1|\op{H}^{(l+1)}|n,l+1}+\frac{1}{(l+1)^2}
  = E_n-E_{l+1}.
\end{gather*}

+++

The remaining states can be found by using the creation operators:

\begin{gather*}
  \ket{n,l} = \frac{\op{A}^{\dagger}_{l}\ket{n,l+1}}{\sqrt{E_{n}-E_{l+1}}}.\\
\begin{aligned}
  &\op{A}_{n-1}\ket{n,n-1} = 0, &
  \braket{r|n,n-1} &= \frac{1}{\sqrt{(2n)!}}\left(\frac{2}{n}\right)^{n+1/2} r^{n}e^{-r/n},\\
  \ket{n,l} &= \prod_{k=1}^{n-l-1} \left(\frac{\op{A}^{\dagger}_{l+k-1}}{\sqrt{E_{n}-E_{l+k}}}\right)\ket{n,n-1},&
  \braket{r|n,l} &= \prod_{k=1}^{n-l-1} \left(
    \frac{\frac{1}{l+k} - \frac{l+k}{r} - \diff{}{r}}{\sqrt{\frac{1}{(l+k)^2} - \frac{1}{n^2}}}
  \right)\braket{r|n,n-1},
  \end{aligned}\\
  \braket{r|n,l} = 
  \frac{2^{n+1}}{n^{l+1}(2n)!}
  \sqrt{\frac{(n+l)!}{(n-l-1)!}}
  \prod_{k=1}^{n-l-1} \left(
    1 - \frac{(l+k)^2}{r} - (l+k)\diff{}{r}
  \right)
  r^{n}e^{-r/n}.
\end{gather*}

+++

## Numerical Solution

+++

Here we present a numerical implementation of the solution.  For the wavefunctions, we use the [Sympy](https://www.sympy.org/en/index.html) symbolic manipulation package.

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
import sympy

m = 0.5
hbar = 1
e2 = 2

# Should not change results if we have included dimensions properly
np.random.seed(2)
m, hbar, e2 = np.random.random(3)+1

N = 2000

m_unit = 2*m
x_unit = hbar**2/m/e2
p_unit = m*e2/2/hbar
E_unit = m*e2**2/2/hbar**2
t_unit = 2*hbar**3/m/e2**2

# Here we use Sympy to compute the radial wavefunctions so we can plot

def get_Unl(n, l):
    """Return (r, U): symbolic radial wavefunctions."""
    r = sympy.symbols('r', positive=True)
    one = sympy.S('1')   # Use symbolic 1 so we have analytic expressions
    U = (2*one/n)**(n+one/2)*r**n*sympy.exp(-r/n)/sympy.sqrt(sympy.factorial(2*n))
    for k in range(n-l-1, 0, -1):
        U = ((one/(l+k) - (l+k)/r)*U - U.diff(r))
        U /= sympy.sqrt(-one/n**2 + one/(l+k)**2)
    return r, U.simplify()

# Memoize for speed with a global cache
_cache = {}

def get_U(r, n, l):
    """Numerical implementation."""
    key = (n, l)
    if key not in _cache:
        _cache[key] = get_Unl(n=n, l=l)
    r_, U_ = _cache[key]
    
    U_r = sympy.lambdify([r_], U_, 'numpy')(r/x_unit)
    U_r /= np.sqrt(x_unit)
    return U_r
    
r = np.linspace(0*x_unit, 200*x_unit, N)
dr = np.diff(r).mean()

r_ = r/x_unit

def get_E(n):
    return -E_unit/n**2

def get_V(r, l):
    r_ = r/x_unit
    return E_unit * (l*(l+1)/r_**2 - 2/r_)
    
# Check orthogonarmality of wavefunctions
# Note: wavefunctions are only orthogonal for a given l 
# - the spherical harmonics ensure orthogonality between different l.
for l in range(5):
    Us = np.array([get_U(r, n=n, l=l)
                   for n in range(l+1, l+5)])
    #print(abs(Us @ Us.T * dr - np.eye(len(Us))).max())
    assert np.allclose(Us @ Us.T * dr, np.eye(len(Us)), atol=1e-5)
```

```{code-cell} ipython3
Nl = 4
Nn = 6
fig, axs = plt.subplots(
    2, Nl, 
    gridspec_kw=dict(height_ratios=[3, 2]),
    figsize=(10, 8))

r = 10.0**(np.linspace(-3, 3, N))*x_unit
r_ = r/x_unit

for l, (ax0, ax1) in enumerate(zip(axs[0], axs[1])):
    V_r = get_V(r=r, l=l)
    ax0.plot(r_, V_r/E_unit, lw=5)

    for s in range(Nn):
        n = 1+s+l
        E_ = get_E(n=n)/E_unit
        U_r = get_U(r=r, n=n, l=l)
        c = f'C{n}'
        ax0.axhline(E_, c=c, ls=':')
        ax1.axhline(np.sqrt(-1/E_), c=c, ls=':')
        ax0.semilogx(r_, U_r*x_unit + E_, c)
        ax1.semilogx(r_, 1.5*U_r*x_unit + np.sqrt(-1/E_), c)
    ax0.set(xlim=(r_[0], r_[-1]), ylim=(-2, 0.2), title=f'$H^{(l)}$')
    ax1.set(xlim=(r_[0], r_[-1]), ylim=(0, 10), xlabel=fr'$r$ [$\hbar^2/me^2$]')
    plt.setp(ax0.get_xticklabels(), visible=False)
    
    if l == 0:
        ax0.set(ylabel='E, V     [$me^2/2\hbar^2$]')
        ax1.set(ylabel=r'$1/\sqrt{-E}$     [$\sqrt{2\hbar^2/me^2}$]')
    else:
        plt.setp(ax0.get_yticklabels(), visible=False)
        plt.setp(ax1.get_yticklabels(), visible=False)

        plt.tight_layout()
```

## Expression in terms of Laguerre Polynomials (Unfinished)

+++

Compare this form with Shankar (13.1.22) (who does not give the normalization):

\begin{gather}
  \rho = \sqrt{\frac{2mE_n}{\hbar^2}}r = \frac{r}{n}, \tag{13.1.6}\\
  R_{nl}(\rho) \propto e^{-\rho} \rho^{l} L_{n-l-1}^{2l+1}(2\rho), \tag{13.1.22}\\
  \braket{r|n,l} = r R_{nl}\left(\frac{r}{n}\right)
  \propto e^{-r/n} r^{l+1} L_{n-l-1}^{2l+1}(2r/n),\\
  L_{p}^{0}(x) = \frac{1}{p!}e^{x}\diff[p]{}{x}(e^{-x}x^{p}), \\
  L_{p}^{k}(x) = (-1)^k\diff[k]{}{x}L^{0}_{p+k} 
               = \frac{(-1)^k}{(k+p)!}\diff[k]{}{x}\left(e^{x}\diff[p+k]{}{x}(e^{-x}x^{p+k})\right).
\end{gather}

+++

An alternative formulation of the [associated Laguerre polynomials](https://en.wikipedia.org/wiki/Laguerre_polynomials#Generalized_Laguerre_polynomials) is a little more useful:

\begin{gather*}
  L_{p}^{k}(x) = \frac{x^{-k}e^{x}}{p!}\diff[p]{}{x}(e^{-x}x^{p+k})\\
  L_{n-l-1}^{2l+1}(2r/n) = \frac{r^{-2l-1}e^{2r/n}}{(n-l-1)!}\diff[n-l-1]{}{r}(e^{-2r/n}r^{n+l}),
\end{gather*}

+++

but I have not yet figured out a simple way of manipulating our expressions into this form:

\begin{gather*}
  \braket{r|n,l} = 
  \frac{2^{n+1}}{n^{l+1}(2n)!}
  \sqrt{\frac{(n+l)!}{(n-l-1)!}}
  r^{-l}e^{r/n}
  \prod_{k=1}^{n-l-1} \left(
    \frac{n-l-k}{n} - \frac{(l+k)k}{r} - (l+k)\diff{}{r}
  \right)
  r^{n+l}e^{-2r/n}.
\end{gather*}

However, I have checked that all of these forms give the correct functional form (and that our forms are indeed properly normalized):

```{code-cell} ipython3
import sympy
r = sympy.var('r', positive=True)

def Ua(n, l):
    one = sympy.S('1')
    f = (2*one/n)**(n+one/2)*r**n*sympy.exp(-r/n)/sympy.sqrt(sympy.factorial(2*n))
    for k in range(n-l-1, 0, -1):
        f = ((one/(l+k) - (l+k)/r)*f - f.diff(r))
        f /= sympy.sqrt(-one/n**2 + one/(l+k)**2)
    return f

def Ub(n, l):
    one = sympy.S('1')
    f = r**n*sympy.exp(-r/n)
    for k in range(n-l-1, 0, -1):
        f = ((1-(l+k)**2/r)*f - (l+k)*f.diff(r))
    f *= 2**(n+1) * sympy.sqrt(sympy.factorial(n+l))
    f /= n**(l+1) * sympy.factorial(2*n)*sympy.sqrt(sympy.factorial(n-l-1))
    return f

def Ub(n, l):
    one = sympy.S('1')
    f = r**(n+l)*sympy.exp(-2*r/n)
    for k in range(n-l-1, 0, -1):
        f = ((n-l-k)/n - (l+k)*k/r)*f - (l+k)*f.diff(r)
    f *= sympy.exp(r/n)*2**(n+1) * sympy.sqrt(sympy.factorial(n+l))
    f /= r**l*n**(l+1)*sympy.factorial(2*n)*sympy.sqrt(sympy.factorial(n-l-1))
    return f

def Ub(n, l):
    one = sympy.S('1')
    f = r**(n+l)*sympy.exp(-2*r/n)
    for k in range(n-l-1, 0, -1):
        f = ((one*n-l-k)/n - (l+k)*k/r)*f - (l+k)*f.diff(r)
    f *= sympy.exp(r/n) * 2**(n+1) * sympy.sqrt(sympy.factorial(n+l))
    f /= r**l*n**(l+1)*sympy.factorial(2*n)*sympy.sqrt(sympy.factorial(n-l-1))
    return f

# Expressions in terms of the Associated Laguerre polynomials
x = sympy.var('x', real=True)
def Lb(p,k):
    """Rodrigues formula."""
    return (sympy.exp(x)*x**(-k)
            *(sympy.exp(-x)*x**(p+k)).diff((x,p))
            /sympy.factorial(p)).simplify()

def Uc(n,l):
    # Not normalized
    rho = r/n
    return sympy.exp(-rho)*rho**(l+1)*Lb(n-l-1,2*l+1).subs(x, 2*rho)

def Ud(n,l):
    # Not normalized
    return r**(-l)*sympy.exp(r/n)*(sympy.exp(-2*r/n)*r**(n+l)).diff((r,n-l-1))

for n in range(1, 6):
    for l in range(n):
        assert (Ua(n,l)/Uc(n,l)).diff(r).simplify() == 0
        assert (Ua(n,l)/Ud(n,l)).diff(r).simplify() == 0
        assert (Ua(n,l)**2).integrate((r,0,'oo')) == 1
        assert (Ub(n,l)**2).integrate((r,0,'oo')) == 1
```

```{code-cell} ipython3
import sympy
x = sympy.var('x', real=True)
def La(p,k):
    """Formula from Shankar with normalization."""
    return (-1)**k*(
        sympy.exp(x)
        *(sympy.exp(-x)*x**(p+k)).diff((x,p+k))
        /sympy.factorial(p+k)
    ).diff((x,k)).simplify()

def Lb(p,k):
    """Rodrigues formula."""
    return (sympy.exp(x)*x**(-k)
            *(sympy.exp(-x)*x**(p+k)).diff((x,p))
            /sympy.factorial(p)).simplify()

for p in range(5):
    for k in range(p):
        assert La(p,k) == Lb(p,k)
```

```{code-cell} ipython3
%pylab inline
m = 0.5
hbar = 1
e2 = 2

# Should not change results if we have included dimensions properly
np.random.seed(2)
m, hbar, e2 = np.random.random(3)+1

N = 200

m_unit = 2*m
x_unit = hbar**2/m/e2
p_unit = m*e2/2/hbar
E_unit = m*e2**2/2/hbar**2
t_unit = 2*hbar**3/m/e2**2

x = np.linspace(-10*x_unit, 10*x_unit, N)
dx = np.diff(x).mean()

x_ = x/x_unit

############# INCOMPLETE
V1 = E_unit * (x_**2 - 1)
W = np.sqrt(E_unit) * x_
dW = E_unit + 0*x_
V2 = V1 + 2*dW

def get_E1(n):
    return E_unit*2*n

# Memoize for speed with a global cache
_cache = {}

@np.vectorize  # Allows us to pass in n as an array.
def get_psi1(x, n):
    global _cache
    key = (x, n)

    if key not in _cache:
        x_ = x/x_unit
        psi0 = np.pi**(-0.25)*np.exp(-x_**2/2)/np.sqrt(x_unit)
        if n == 0:
            psi = psi0
        elif n == 1:
            psi = np.sqrt(2)*x_*psi0
        else:
            psi = np.sqrt(2/n)*x_*get_psi1(x, n-1) - np.sqrt((n-1)/n)*get_psi1(x, n-2)
        _cache[key] = psi
    return _cache[key]

def get_psi2(x, n):
    return get_psi1(x, n-1)

# Check orthogonarmality of wavefunctions
n = np.arange(10)
psi1s = get_psi1(x[:, None], n[None, :])
assert np.allclose(psi1s.T @ psi1s * dx, np.eye(len(n)))
```

```{code-cell} ipython3

```
