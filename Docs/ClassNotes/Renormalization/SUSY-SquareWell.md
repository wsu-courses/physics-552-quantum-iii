---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.7
kernelspec:
  display_name: Python [conda env:work]
  language: python
  name: conda-env-work-py
---

```{code-cell}
:hide-cell: null

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

**A note about notation.** To simplify the notation, we change conventions slightly.
SUSY pairs will be denoted by: 

\begin{gather*}
  \op{H}_{-} = \op{A}^\dagger\op{A}, \qquad
  \op{H}_{+} = \op{A}\op{A}^\dagger.
\end{gather*}

These were previously denoted by $\op{H}_1$ and $\op{H}_2$ respectively.  We then use the notation:

\begin{gather*}
  \op{H}_{s} = \op{A}_s^\dagger\op{A}_s + E_s\op{1}
\end{gather*}

to denote the SUSY hierarchy.  The operator $\op{A}_s$ moves across this:

\begin{gather*}
  \ket{n}_{s+1} = \frac{\op{A}_s\ket{n}_s}{\sqrt{E_n-E_s}}, \qquad
  \braket{n|n}_{s+1} = \frac{\braket{n|\op{A}_s^\dagger\op{A}_s|n}_{s+1}}{E_n - E_s}
                     = \frac{\braket{n|\op{H}_s - E_s\op{1}|n}_{s+1}}{E_n - E_s} = 1,
\end{gather*}

where $\ket{n=s}_s$, the ground state of $\op{H}_s$, is annihilated: $\op{A}_s\ket{s}_s = 0$.

+++

# Applied SUSY: Infinite Square Well

+++

Here we use SUSY to extend the solved problem of an infinite square well:

\begin{gather*}
  \op{H} = \frac{\op{p}^2}{2m} + V(\op{x}), \qquad
  V(x) = \begin{cases}
    0 & 0 < x < L\\
    \infty & \text{otherwise}
  \end{cases}, \\
  \psi_n(x) = \braket{x|n} = \sqrt{\frac{2}{L}}\sin\frac{\pi (1+n) x}{L}, \qquad
  E_n = (1+n)^2\frac{\pi^2\hbar^2}{2mL^2}.
\end{gather*}

+++

## Units
We choose units $\hbar = 2m = L/\pi = 1$.

\begin{gather*}
  [\hbar] = \frac{MD^2}{T}, \qquad [2m] = M, \qquad [L/\pi] = D.
\end{gather*}

To reconstruct the dimensionful solution, insert 1 in the following forms to get the correct dimensions:

\begin{gather*}
  1 = \underbrace{\frac{2mL^2}{\pi^2\hbar}}_{\text{time}}
    = \underbrace{\frac{L}{\pi}}_{\text{distance}}
    = \underbrace{\frac{\pi^2\hbar^2}{2mL^2}}_{\text{energy}}
    = \underbrace{\frac{\pi\hbar}{L}}_{\text{momentum}}.
\end{gather*}

The solution in these units is thus:

\begin{gather*}
  \op{H} = \op{p}^2 + V(\op{x}), \qquad
  V(x) = \begin{cases}
    0 & 0 < x < \pi\\
    \infty & \text{otherwise}
  \end{cases}, \\
  \psi_n(x) = \braket{x|n} = \sqrt{\frac{2}{\pi}}\sin\bigl((1+n) x\bigr), \qquad
  E_n = (1+n)^2.
\end{gather*}

+++

## SUSY Relations

We will now form the SUSY hierarchy $\op{H}_{s}$ for $s\in [0, 1, 2, \dots]$:

\begin{align}
  \op{H} = 
  \op{H}_0 &= \op{A}_0^\dagger\op{A}_0 + E_0\op{1} & \{E_0, E_1, E_2, E_3, \dots\}&\\
  \op{H}_1 &= \op{A}_1^\dagger\op{A}_1 + E_1\op{1}
            = \op{A}_0\op{A}_0^\dagger + E_0\op{1} & \{E_1, E_2, E_3, \dots\}&\\
  \op{H}_2 &= \op{A}_2^\dagger\op{A}_2 + E_2\op{1}
            = \op{A}_1\op{A}_1^\dagger + E_1\op{1} & \{ E_2, E_3, \dots\}&\\
            &\vdots
\end{align}

+++

Since we know the eigenstates for $\op{H}$, we use these to construct the superpotentials:

\begin{gather*}
  \begin{aligned}
    W_0(x) = -\frac{\psi_0'(x)}{\psi_0(x)} &= -\cot x, &
             V_-(x) &= \cot^2 x - \csc^2{x} = -1,\\
    W_0'(x) &= \csc^2{x}, &
    V_+(x) &= \cot^2 x + \csc^2{x} = 2\csc^2{x} - 1,\\
    \op{H}_0 - E_0\op{1} = \op{H}_{-} &= \op{p}^2 - 1, &
    \op{A}_0 &\rightarrow -\cot x + \diff{}{x}, \\
    \op{H}_1 - E_0\op{1} = \op{H}_{+} &= \op{p}^2 + 2\csc^2\op{x} - 1.
  \end{aligned}
\end{gather*}

This gives us the next Hamiltonian in our chain, and its eigenstates:

\begin{gather*}
  \op{H}_1 = \op{p}^2 + 2\csc^2{x}, \qquad
  \braket{x|n}_1 = \frac{\braket{x|\op{A}_0|n}_0}{\sqrt{E_n-E_0}} 
                 = \sqrt{\frac{2}{\pi n(n+2)}}\left(
                    n \cos\bigl((1+n)x\bigr) - \frac{\sin(nx)}{\sin x}
                \right).
\end{gather*}

+++

From the ground state $\ket{1}_1$ of $\op{H}_1$ we start the next pair:

\begin{gather*}
  -\braket{x|1}_1 = \sqrt{\frac{2}{3\pi}}\bigl(1-\cos(2x)\bigr)
                 = \sqrt{\frac{8}{3\pi}}\sin^2 x, \\
  \begin{aligned}
    W_1(x) = \frac{-\braket{x|1}_1'}{\braket{x|1}_1} &= -2\cot x, &
    V_-(x) &= 2\csc^2{x} - 4, \\
    W_1'(x) &= 2\csc^2 x, &
    V_+(x) &= 6\csc^2{x} - 4,\\
    \op{H}_1 - E_1\op{1} = \op{H}_{-} &= \op{p}^2 + 2\csc^2\op{x} - 4, &
                             \op{A}_1 &\rightarrow -2\cot x + \diff{}{x}, \\
    \op{H}_2 - E_1\op{1} = \op{H}_{+} &= \op{p}^2 + 6\csc^2{x} - 4.
  \end{aligned}
\end{gather*}

This gives us the next Hamiltonian in our chain, and its eigenstates:

\begin{gather*}
   \op{H}_2 = \op{p}^2 + 6\csc^2{x}, \qquad
   \braket{x|n}_2 = \frac{\braket{x|\op{A}_1|n}_1}{\sqrt{E_n-E_1}} 
   = \sqrt{\frac{2}{\pi n(n+2)(n-1)(n+3)}}\left(
                 -n(n+2)\sin\bigl((1+n)x\bigr)
                 +\frac{3\sin\bigl((1+n)x\bigr)}{\tan^2 x}
                 -\frac{3(1+n)\cos\bigl((1+n)x\bigr)}{\tan x}
                \right).
\end{gather*}

```{code-cell} ipython3
import numpy as np
x_ = np.linspace(0,np.pi, 200)[1:-1][None, :]
n_ = np.arange(2, 10)[:, None]
s1, c1 = np.sin((1+n_)*x_), np.cos((1+n_)*x_)
t = np.tan(x_)
psis2 = (-n_*(n_+2)*s1 + 3*s1/t**2
         - 3*(1+n_)*c1/t
        )*np.sqrt(2/np.pi/n_/(n_+2)/(n_-1)/(n_+3))
assert np.allclose(psis2 @ psis2.T * np.diff(x_).mean(), np.eye(8))
```

```{code-cell} ipython3
# Some sympy code for implementing this proceedure.
# Simplification does not work too well.
import sympy
normalize = True
sympy.init_printing()

def my_simp(expr):
    return sympy.expand_trig(expr.simplify()).trigsimp().simplify()

x = sympy.var('x', real=True)
n = sympy.var('n', integer=True)
psis0 = sympy.sin((1+n)*x)
if normalize:
    psis0 *= sympy.sqrt(2/sympy.pi)
Es = (1+n)**2
psi0_0 = psis0.subs(n, 0)
W0 = (-psi0_0.diff(x)/psi0_0).simplify()
dW0 = W0.diff(x).simplify()
V0 = W0**2 - dW0 + Es.subs(n,0)
psis1 = (W0*psis0 + psis0.diff(x)).simplify()
if normalize:
    psis1 /= sympy.sqrt(Es-Es.subs(n,0))
else:
    assert my_simp(psis1.subs(n,0)) == 0

psi1_0 = my_simp(psis1.subs(n, 1))
W1 = (-psi1_0.diff(x)/psi1_0).simplify()
dW1 = W1.diff(x).simplify()
V1 = W1**2 - dW1 + Es.subs(n,1)

psis2 = (W1*psis1 + psis1.diff(x)).simplify()
if normalize:
    psis2 /= sympy.sqrt(Es-Es.subs(n,1))
else:
    assert my_simp(psis2.subs(n,1)) == 0
    
psi2_0 = my_simp(psis2.subs(n, 2))
W2 = my_simp(-psi2_0.diff(x)/psi2_0)
dW2 = W2.diff(x).simplify()
V2 = W2**2 - dW2 + Es.subs(n,2)
```

```{code-cell} ipython3
%pylab inline
fig, axs = plt.subplots(1, 3, figsize=(10,8))
x_ = np.linspace(0, np.pi, 100)[1:-1]
for _s, (ax, V, psis) in enumerate(
        zip(axs, [V0, V1, V2], [psis0, psis1, psis2])):
    V_ = sympy.lambdify([x], V, 'numpy')
    psi_ = sympy.lambdify([x, n], psis, 'numpy')
    ax.plot(x_, V_(x_), lw=5)
    for _n in range(_s, 9):
        c = f'C{_n+1}'
        E = Es.subs(n, _n)
        ax.axhline(E, c=c, ls=':')
        ax.plot(x_, (-1)**(_s)*psi_(x_, _n) + E, c)
    ax.set(xlim=(0, np.pi), ylim=(-1, 20), xlabel=f'$H_{_s}$')
    if _s == 0:
        ax.set(ylabel='E, V')
    else:
        plt.setp(ax.get_yticklabels(), visible=False)
```
